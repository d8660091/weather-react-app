module.exports = {
  "Test home page": function(browser) {
    const devServer = browser.globals.devServerURL;

    browser
      .url(devServer)
      .waitForElementVisible(".navbar button", 2000)
      .setValue(".navbar input", "Ottawa")
      .click(".navbar button")
      .useXpath()
      .waitForElementVisible('//h4[contains(text(), "Ottawa")]', 3000)
      .useCss()
      .assert.attributeContains(
        ".App-weekday:first-child",
        "class",
        "App-weekday--active"
      )
      .end();
  }
};
