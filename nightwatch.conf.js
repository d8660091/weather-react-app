module.exports = {
  src_folders: ["tests"],
  output_folder: "reports",
  custom_commands_path: "",
  custom_assertions_path: "",
  page_objects_path: "",
  globals_path: "",

  selenium: {
    start_process: true,
    server_path: require("selenium-server").path,
    host: "127.0.0.1",
    port: 4444,
    cli_args: {
      "webdriver.chrome.driver": require("chromedriver").path
    }
  },

  test_settings: {
    default: {
      launch_url: "http://localhost",
      selenium_port: 4444,
      selenium_host: "localhost",
      silent: true,
      desiredCapabilities: {
        browserName: "chrome",
        marionette: true
      },
      globals: {
        devServerURL: "http://localhost:3000"
      }
    },

    chrome: {
      desiredCapabilities: {
        browserName: "chrome"
      }
    }
  }
};
