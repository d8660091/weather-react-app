[![pipeline status](https://gitlab.com/d8660091/weather-react-app/badges/master/pipeline.svg)](https://gitlab.com/d8660091/weather-react-app/commits/master)

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

staging server: https://weather-cb20b.firebaseapp.com/

# How to start

``` shell
yarn start
```

# Features
### Basic
- [x] Weather basic info in 5 days with icons.
- [x] Weather details for each day.

### Advanced
- [x] Chart with hoursly temperature.
- [x] Average pressure. 
- [x] City autocompletion.
- [x] Gitlab CI/CD


# Dependencies
[Lodash](https://lodash.com/docs/4.17.4)

[Recompose](https://github.com/acdlite/recompose/blob/master/docs/API.md)

[Chart.js](http://www.chartjs.org)

[Moment](https://momentjs.com/)

[Bootstrap](https://getbootstrap.com/)

# Dev tools
Emacs, Prettier
