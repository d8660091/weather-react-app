// import fakeData from './fakeData' // Openweathermap Free plan is too limited

const appId = "c7287ab86c9b76839b15f0a1bced845a";

export async function getWeatherForecast({ cityName }) {
  const response = await fetch(
    `https://api.openweathermap.org/data/2.5/forecast?q=${cityName}&APPID=${appId}`
  );

  const data = await response.json();
  return data;

  // return fakeData;
}
