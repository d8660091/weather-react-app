import React from "react";
import Autocomplete from "react-autocomplete";
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withProps,
  withPropsOnChange
} from "recompose";
import {
  get,
  groupBy,
  toPairs,
  min,
  max,
  find,
  map,
  take,
  property,
  sortBy,
  identity,
  ary
} from "lodash-es";
import moment from "moment";
import classNames from "classnames";

import rings from "./rings.svg";
import "./App.css";
import TempLineChart from "./TempLineChart";
import { getWeatherForecast } from "./weatherFetcher";

const zeroKelvin = -273.15;

// {forecasts} (groupBy)=> {weekday: [forecasts]} (toPairs)=> [[weekday, [forecasts]]] (map)=> [{weekday: number, high: number, low:number, forecasts: [forecasts]}]
function formatForecast(list = []) {
  // group by week day and calculate high, low temperatures
  return sortBy(
    toPairs(groupBy(list, d => moment.unix(d.dt).day())).map(
      ([weekday, forecasts]) => ({
        weekday,
        forecasts,
        temp_min: min(forecasts.map(forecast => forecast.main.temp)),
        temp_max: max(forecasts.map(forecast => forecast.main.temp))
      })
    ),
    "forecasts[0].dt"
  );
}

function formatKelvin(kelvin) {
  return Math.round(kelvin + zeroKelvin) + "°";
}

const enhance = compose(
  withState("cityName", "setCityName", ""),
  withState("forecastData", "receiveForecastData", null),
  withState("error", "setError", null),
  withPropsOnChange(["forecastData"], props => {
    return {
      formatedForecastData: formatForecast(get(props.forecastData, "list"))
    };
  }),
  withState("isLoading", "setIsLoading", false),
  withState(
    "activeWeekday",
    "setActiveWeekday",
    moment()
      .day()
      .toString()
  ),
  withProps(props => {
    return {
      activeWeekdayForecasts: take(
        [
          ...get(
            find(props.formatedForecastData, {
              weekday: props.activeWeekday
            }),
            "forecasts",
            []
          ),
          ...get(
            find(props.formatedForecastData, {
              weekday: ((parseInt(props.activeWeekday, 10) + 1) % 7).toString()
            }),
            "forecasts",
            []
          )
        ],
        6
      )
    };
  }),
  withHandlers({
    onCityNameChange: props => event => {
      props.setCityName(event.target.value);
    },
    onWeekdayClick: props => weekday => event => {
      props.setActiveWeekday(weekday);
    },
    onSearchClick: props => async event => {
      event && event.preventDefault();
      props.setIsLoading(true);
      props.setError(null);
      const data = await getWeatherForecast({
        cityName: props.cityName || "Toronto"
      });
      props.receiveForecastData(data);
      if (data.cod === "404") {
        props.setError(data.message);
      }
      props.setIsLoading(false);
    }
  }),
  lifecycle({
    async componentDidMount() {
      this.props.onSearchClick();
    }
  })
);

function App(props) {
  return (
    <div className="App">
      <nav className="navbar navbar-light bg-light justify-content-center">
        <a className="navbar-brand">5-day Weather Forcast</a>
        <form className="form-inline" onSubmit={props.onSearchClick}>
          <Autocomplete
            getItemValue={identity}
            items={["Toronto", "Ottawa", "Montreal", "Vancouver", "Waterloo"]}
            value={props.cityName}
            onChange={props.onCityNameChange}
            onSelect={ary(props.setCityName, 1)}
            shouldItemRender={item =>
              item.toLowerCase().includes(props.cityName.toLowerCase())
            }
            renderItem={function(item, isHighlighted) {
              return (
                <div
                  key={item}
                  className="dropdown-item"
                  style={{
                    paddingLeft: "12px",
                    background: isHighlighted ? "#f0f2f4" : "transparent"
                  }}
                >
                  {item}
                </div>
              );
            }}
            renderMenu={function(items, value, style) {
              return (
                <div
                  className="dropdown-menu d-block"
                  style={{ ...style, ...this.menuStyle }}
                  children={items}
                />
              );
            }}
            renderInput={props => <input {...props} className="form-control" />}
          />
          <button
            type="button"
            className="btn btn-outline-success m-2 my-sm-0"
            onClick={props.onSearchClick}
          >
            Search
          </button>
        </form>
      </nav>
      {props.isLoading && (
        <div className="container mt-5 text-center">
          <img alt="loading" src={rings} />
        </div>
      )}
      {props.error && (
        <div className="container mt-5">
          <h3 className="text-danger">{props.error}</h3>
        </div>
      )}
      {!props.isLoading &&
        !props.error && (
          <div className="container mt-2 mt-4">
            <h4>
              {get(props.forecastData, "city.name")},{" "}
              {get(props.forecastData, "city.country")}
            </h4>
            <h4>
              {moment()
                .weekday(props.activeWeekday)
                .format("dddd")},{" "}
              {get(props.activeWeekdayForecasts, "[0].weather[0].description")}
            </h4>
            <div className="d-flex flex-row align-items-center">
              <img
                alt={get(
                  props.activeWeekdayForecasts,
                  "[0].weather[0].description"
                )}
                src={`https://openweathermap.org/img/w/${get(
                  props.activeWeekdayForecasts,
                  "[0].weather[0].icon",
                  "01n"
                )}.png`}
              />
              <span style={{ fontSize: "1.5em" }}>
                {formatKelvin(
                  max(map(props.activeWeekdayForecasts, property("main.temp")))
                )}C
              </span>
            </div>
            <dl className="row mt-4">
              <dt className="col-sm-3">Wind speed</dt>
              <dd className="col-sm-9">
                {get(props.activeWeekdayForecasts, "[0].wind.speed")} m/s
              </dd>
              <dt className="col-sm-3">Humidity</dt>
              <dd className="col-sm-9">
                {get(props.activeWeekdayForecasts, "[0].main.humidity")} %
              </dd>
              <dt className="col-sm-3">5-days Average Pressure</dt>
              <dd className="col-sm-9">
                {get(props.forecastData, "list", [])
                  .reduce((acc, { main: { pressure } }, _idx, arr) => {
                    return acc + pressure / arr.length;
                  }, 0)
                  .toFixed(2)}
              </dd>
            </dl>
            <div className="pt-4" style={{ height: "400px" }}>
              <TempLineChart
                key={
                  get(props.forecastData, "city.id") + "_" + props.activeWeekday
                }
                min={
                  min(
                    map(get(props.forecastData, "list"), property("main.temp"))
                  ) + zeroKelvin
                }
                data={{
                  labels: map(props.activeWeekdayForecasts, d =>
                    moment.unix(d.dt).format("h A")
                  ),
                  datasets: [
                    {
                      backgroundColor: "#FFF5CC",
                      borderColor: "#FFCC00",
                      moment,
                      data: map(
                        props.activeWeekdayForecasts,
                        d => d.main.temp + zeroKelvin
                      )
                    }
                  ]
                }}
              />
            </div>
            <div className="App-weekday-tabs">
              {props.formatedForecastData.map(d => {
                return (
                  <div
                    className={classNames("App-weekday", {
                      "App-weekday--active": props.activeWeekday === d.weekday
                    })}
                    key={`${props.cityName}_${d.weekday}`}
                    onClick={props.onWeekdayClick(d.weekday)}
                  >
                    <div>
                      {moment()
                        .weekday(d.weekday)
                        .format("ddd")}
                    </div>
                    <div>
                      <img
                        alt={d.forecasts[0].weather[0].description}
                        src={`https://openweathermap.org/img/w/${
                          d.forecasts[0].weather[0].icon
                        }.png`}
                      />
                    </div>
                    <div>
                      <span className="mr-2">{formatKelvin(d.temp_max)}</span>
                      <span className="text-muted">
                        {formatKelvin(d.temp_min)}
                      </span>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        )}
    </div>
  );
}

export default enhance(App);
