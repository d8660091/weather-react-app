import React from "react";
import { compose, lifecycle, withHandlers } from "recompose";
import Chart from "chart.js";
import { isEqual } from "lodash-es";

const enhance = compose(
  withHandlers(props => {
    let canvasRef;
    let chart;
    return {
      setCanvasRef: () => r => {
        canvasRef = r;
      },
      setUpChart: props => () => {
        chart = new Chart(canvasRef, {
          type: "line",
          data: props.data,
          options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
              display: false
            },
            scales: {
              yAxes: [
                {
                  display: true,
                  ticks: {
                    suggestedMin: props.min
                  },
                  scaleLabel: {
                    display: true,
                    labelString: "Temperature (°C)"
                  }
                }
              ]
            }
          }
        });
      },
      updateChart: props => () => {
        chart.data = props.data;
        chart.update();
      }
    };
  }),
  lifecycle({
    componentDidMount() {
      this.props.setUpChart();
    },
    componentDidUpdate(prevProps) {
      if (!isEqual(prevProps.key, this.props.key)) {
        this.props.updateChart();
      }
    }
  })
);

function TempLineChart(props) {
  return <canvas ref={props.setCanvasRef}>TempLineChart</canvas>;
}

export default enhance(TempLineChart);
